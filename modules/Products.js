import axios from 'axios'
export default class Products {
  static async loadProducts(context) {
    if (context.store.state.products) {
      return context.store.state.products
    } else {
      const responce = await axios.get(
        `https://jsonplaceholder.typicode.com/posts`
      )
      return responce.data
    }
  }

  static async addPost(context, payload) {
    const responce = await axios.post(
      'https://jsonplaceholder.typicode.com/posts',
      {
        title: payload.title,
        body: payload.body,
        userId: 1,
      }
    )
    return responce.data
  }

  static async getPost(context, id) {
    const responce = await axios.get(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
    )
    return responce.data
  }
  static async editPost(context, payload) {
    const responce = await axios.get(
      `https://jsonplaceholder.typicode.com/posts/${payload.id}`,
        {
          id: payload.id,
          title: payload.title,
          body: payload.body,
          userId: payload.userId,
        }
    )
    return responce.data
  }
  static async deletePost(context, id) {
    const responce = await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
    )
    return responce.data
  }
}

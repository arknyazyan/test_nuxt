export { default as Form } from '../../components/Form.vue'
export { default as Header } from '../../components/Header.vue'
export { default as List } from '../../components/List.vue'
export { default as Logo } from '../../components/Logo.vue'

export const LazyForm = import('../../components/Form.vue' /* webpackChunkName: "components/form" */).then(c => c.default || c)
export const LazyHeader = import('../../components/Header.vue' /* webpackChunkName: "components/header" */).then(c => c.default || c)
export const LazyList = import('../../components/List.vue' /* webpackChunkName: "components/list" */).then(c => c.default || c)
export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/logo" */).then(c => c.default || c)

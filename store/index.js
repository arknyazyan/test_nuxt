export const strict = false
export const state = () => ({
  products: null,
})

export const mutations = {
  SET_PRODUCTS(state, payload) {
    state.products = payload
  },
  ADD_POST(state, payload) {
    state.products.push(payload)
  },
  EDIT_POST(state, payload) {
    state.products = payload
  },
  REMOVE_POST(state, payload) {
    state.products = payload
  },
}
export const getters = {
  getProducts: (state) => {
    return state.products
  },
}

export const actions = {}
